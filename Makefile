build:
	$(CC) -o libwrite_sync.so write.c -shared

install:
	mkdir -p $(DESTDIR)/usr/libexec/
	install libwrite_sync $(DESTDIR)/usr/libexec/libwrite_sync.so
clean:
	rm -f libwrite_sync.so